/*
	AUTHOR: Jérôme 
*/
/*DROP*/

DROP TABLE IF exists detailCommande ; 
DROP TABLE IF exists commande ;
DROP TABLE IF exists kitComposant ;
DROP TABLE IF exists kitcote ;
DROP TABLE IF exists kit ;
DROP TABLE IF exists produit ;
DROP TABLE IF exists item ;
DROP TABLE IF exists client ;
DROP TABLE IF exists adresse ;
DROP TABLE IF exists personne ;



/*CREATE*/
PRAGMA foreign_keys = ON ; 

CREATE TABLE adresse(
	id_adresse integer  primary key AUTOINCREMENT, 
	numero integer not null, 
	rue varchar(255) not null, 
	ville varchar(255) not null,  
	complement text , 
	codePostal varchar(255)not null, 
	pays varchar(255) not null,
	tel varchar(12) not null
);

CREATE TABLE Personne(
	id_personne integer  primary key autoincrement, 
	nom_personne varchar(100) not null, 
	prenom_personne varchar(100) not null, 
	email_personne varchar(255) not null , 
	passwdHash varchar(64) not null, 
	id_adrsFacturation integer ,
	id_adrsLivraison integer  ,
	societe varchar(255),
	role_personne text not null DEFAULT('CREATEUR') CHECK (role_personne in ('ADMIN','CREATEUR')) ,
	CONSTRAINT email_unique_var UNIQUE (email_personne),
	FOREIGN KEY(id_personne) REFERENCES personne(id_personne), 
	FOREIGN KEY(id_adrsFacturation) REFERENCES adresse(id_adresse), 
	FOREIGN KEY(id_adrsLivraison) REFERENCES adresse(id_adresse)
);
/*
CREATE TABLE client (
	id_personne integer not null ,
	id_adrsFacturation integer not null ,
	id_adrsLivraison integer not null ,
	FOREIGN KEY(id_personne) REFERENCES personne(id_personne), 
	FOREIGN KEY(id_adrsFacturation) REFERENCES adresse(id_adresse), 
	FOREIGN KEY(id_adrsLivraison) REFERENCES adresse(id_adresse)
);*/

CREATE TABLE item(
	id_item integer primary key AUTOINCREMENT ,
	libelle varchar(255) not null, 
	type varchar(10) not null CHECK ( type in ('COFFRET', 'KIT', 'PRODUIT'))
);
CREATE TABLE produit(
	id_produit integer not null  , 
	qteParArticle integer not null DEFAULT(1),
	stock integer not null, 
	poidPackaging double not null, 
	prixPackaging REAL not null, 
	refFourniseurArticle text not null, 
	CONSTRAINT id_produit_unique UNIQUE (id_produit),
	FOREIGN KEY(id_produit) REFERENCES item(id_item)
);

CREATE TABLE kit( 
	id_kit integer not null, 
	id_personne integer not null,
	kit_like integer not null default(0),
	kit_dislike integer not null default(0), 
	CONSTRAINT id_kit_unique UNIQUE (id_kit),
	FOREIGN key( id_personne) REFERENCES personne(id_personne)
); 
CREATE TABLE kitcote(
	id_personne integer not null, 
	id_kit integer not null,
	type varchar(255) not null CHECK (type in ('LIKE', 'DISLIKE')),
	FOREIGN key (id_personne) REFERENCES personne(id_personne),
	FOREIGN key (id_kit) references kit(id_kit),
	primary key('id_personne','id_kit','type')
);
CREATE TABLE kitComposant(
	id_kit integer not null ,  
	id_produit integer not null, 
	quantite integer not null DEFAULT(1),
	FOREIGN KEY(id_kit) REFERENCES kit(id_kit), 
	FOREIGN KEY(id_produit) REFERENCES produit(id_produit)
);

CREATE TABLE commande(
	id_commande integer primary key AUTOINCREMENT , 
	id_client integer not null, 
	FOREIGN KEY(id_client) REFERENCES personne(id_personne)
);
CREATE TABLE detailCommande(
	id_cmd integer not null,
	id_item integer not null,
	qte integer not null, 
	FOREIGN KEY(id_cmd) REFERENCES commande(id_commande),
	FOREIGN KEY(id_item) REFERENCES item(id_item)
);
/* requete*/
