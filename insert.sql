DELETE FROM detailCommande ; 
DELETE FROM commande ;
DELETE FROM kitComposant ;
DELETE FROM kitcote ;
DELETE FROM kit ;
DELETE FROM produit ;
DELETE FROM item ;

DELETE FROM personne ;
DELETE FROM adresse ;

INSERT INTO adresse(numero,rue,ville,complement,codePostal,pays,tel) VALUES 
	('8320', 'Bunker Hill', 'Gakovo', 'Shang Boingor', '4004', 'Serbia', '216-143-4877'),
	('4', 'Michigan', 'Maputi', 'Shaft', '9024', 'Philippines', '749-515-0320'),
	('10241', 'Graceland', 'Reszel', 'Real', '11-440', 'Poland', '706-151-9778'),
	('5886', 'Luster', 'Figueira', 'Haibara', '8650-153', 'Portugal', '300-349-3921'),
	('2', 'Oak Valley', 'Nakonde', 'Sumber Tengah', '68844', 'Zambia', '641-388-2896');

INSERT INTO personne(nom_personne,prenom_personne,societe,role_personne,email_personne,passwdHash, id_adrsFacturation,id_adrsLivraison) VALUES 
	('Doe','John','JD','ADMIN','haha2@laposte.net','admin',(select max(id_adresse) from adresse),(select max(id_adresse) from adresse)),
	('Rockwell', 'Glavis', 'Linktype', 'CREATEUR','haha1@laposte.net','admin',(select max(id_adresse) from adresse),(select max(id_adresse) from adresse)),
	('Maren', 'Sacks', 'Meezzy', 'CREATEUR','haha3@laposte.net','admin',(select max(id_adresse) from adresse),(select max(id_adresse) from adresse)),
	('Benny', 'Bittlestone', 'Eare', 'CREATEUR','haha4@laposte.net','admin',(select max(id_adresse) from adresse),(select max(id_adresse) from adresse)),
	('Archie', 'Halward', 'Quatz', 'CREATEUR','haha5@laposte.net','admin',(select max(id_adresse) from adresse),(select max(id_adresse) from adresse));

INSERT INTO item(type,libelle) VALUES 
	('KIT', 'My beau KIT'),
	('KIT', 'si si la famille'),
	('COFFRET','jolie coffret de rien'),
	('PRODUIT','boucle oreil'),
	('PRODUIT','cuir'),
	('PRODUIT','Cava'),
	('PRODUIT','COUCOU'),
	('PRODUIT','post-it'),
	('PRODUIT','cafe');

INSERT INTO produit VALUES 
	((select max(id_item) from item) , 1,17,10.5,5.1,'K117105'),
	((select max(id_item)-1 from item) , 1,23,15.32,7.49,'C123157'),
	((select max(id_item)-2 from item) , 1,17,10.12,5.99,'K117105'),
	((select max(id_item)-3 from item) , 1,17,10.56,5.31,'K117105'),
	((select max(id_item)-4 from item), 1,17,10.2,5.3,'K117105'),
	((select max(id_item)-5 from item), 1,17,10.3,5.5,'K117105');


INSERT INTO kit(id_kit, id_personne) VALUES 
	((select max(id_item) from item)-6 ,1),
	((select max(id_item) from item)-7,1),
	((select max(id_item) from item)-8,1);


INSERT INTO kitComposant VALUES 
	((select max(id_item)-7 from item) , (select max(id_item)-1 from item) ,10),
	((select max(id_item)-7 from item) , (select max(id_item)-2 from item) ,6),
	((select max(id_item)-7 from item) , (select max(id_item)-3 from item) ,7),
	((select max(id_item)-7 from item) , (select max(id_item)-4 from item) ,5);
INSERT INTO commande(id_client) VALUES 
	((select max(id_personne) from personne));
INSERT INTO detailCommande VALUES 
	((select max(id_commande) from commande), (select max(id_item)-3 from item) , 2),
	((select max(id_commande) from commande), (select max(id_item)-2 from item) , 2),
	((select max(id_commande) from commande), (select max(id_item)-7 from item) , 2);





