# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons réussi à afficher les détails des commandes, à ajouter et supprimer des kits et à  ajouter des utilisateurs en fonction d'une adresse mail. Nous avons tenté de parser les données sur l'application, et nous avons créé une page d'inscription bientôt opérationnelle. Nous avons réussi à ajouter des personnes sans qu'il y ait de doublon d'email.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons parser les données sur l'application, et aussi faire en sorte qu'un utilisateur puisse se connecter. Nous allons créer des kits, des produits et des personnes.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'affichage des commandes, kits, produits et personnes.

### Qu'avons nous observé ? 
Cela fonctionne.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons continuer d'afficher les données et en créer des nouvelles.

### Qu'allons nous tester durant les 2 prochaines heures ? 
L'affichage de la SPA, le parsing, des données, leur protection aussi.

### À quoi verra-t-on que celà à fonctionné ?
Si le tout s'affiche sans problème.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
