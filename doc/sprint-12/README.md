# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons modifié la requête REST pour l'ajout des produits sur le site web et dans la base de données. Nous avons aussi modifié le style de la SPA. Nous avons mis à jour le protocole de l'API. Au niveau de l'application mobile, nous pouvons afficher les kits ainsi que le détail de leur contenu et le client peut se connecter ou s'inscrire.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons implémenter le protocole de l'API, modifier le style de la SPA et continuer de développer l'application mobile. Nous allons essayer de rassembler le tout.


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le style de la SPA, l'affichage des kits sur l'application et l'ajout de produits sur le site web.

### Qu'avons nous observé ? 
Lorsqu'on réduit la page, celle-ci se met bien en forme. Les kits s'affichent bien sur l'application mobile, nous pouvons aussi visualiser le détail de leur contenu. Si l'utilisateur ne rentre pas les bonnes informations ou ne rentre rien du tout l'accès lui est refusé.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons continuer d'optimiser le tout.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons faire ce que nous avons dit précédemment.

### À quoi verra-t-on que celà à fonctionné ?
S'il n'y a pas d'erreurs, de bugs...

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
