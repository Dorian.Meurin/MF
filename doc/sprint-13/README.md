# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous pouvons modifier le stock sur le site web, sinscrire sur l'application mobile.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons réaliser d'autres requêtes REST pour l'ajout, la modification et la suppression de données. Nous allons aussi toujours continuer de modifier la SPA.


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'inscription d'un utilisateur et l'affichage de ses données. 


### Qu'avons nous observé ? 
L'inscription marche bien en revanche nous n'avons pas réussi à afficher les données de l'utilisateur.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons régler ce problème.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tester ce que nous avons cité précédemment.

### À quoi verra-t-on que celà à fonctionné ?
S'il n'y a pas d'erreurs, de bugs...

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
