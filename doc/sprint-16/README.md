# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous pouvons désormais remplir des kits sur le site web et les utilisateurs peuvent se connecter à partir de l'application mobile. Nous avons tenté de faire le docker.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons nous concentrer sur le graphisme, parser les données CSV en json, faire le docker et créer des kits. 


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nous avons encore testé le style de la SPA, l'importation de données à partir d'un fichier CSV, la page de chargement  et la création de kits dans l'application mobile. 


### Qu'avons nous observé ? 
L'importation de données n'est pas encore opérationnelle, ainsi que le docker.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons trouver d'autres solutions pour que le tout soit fonctionnel à la fin.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tout tester.

### À quoi verra-t-on que celà à fonctionné ?
S'il n'y a pas d'erreurs, de bugs, d'anomalies.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
