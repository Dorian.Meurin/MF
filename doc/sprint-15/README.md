# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons modifié le style de la SPA. Nous avons essayé d'envoyer au serveur les données contenues dans un fichier CSV. Nous avons créé une page de chargement dans l'application mobile.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons encore modifier le style de la SPA, régler l'importation de fichiers afin que les données soient envoyées au serveur et enregistrées dans la base de données. Nous allons ajouter la fonctionnalité "Créer des kits" dans l'application.


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le style de la SPA, l'importation de données à partir d'un fichier CSV, la page de chargement  et la création de kits dans l'application mobile.


### Qu'avons nous observé ? 
L'importation de données n'est pas du tout opérationnelle, ainsi que la création de kits.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons trouver une autre solution.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons continuer le développement de l'application mobile et de la SPA.

### À quoi verra-t-on que celà à fonctionné ?
S'il n'y a pas d'erreurs, de bugs...

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
