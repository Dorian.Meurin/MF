# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons réalisé la notation des kits dans l'application mobile à l'aide des boutons "like" et "dislike". Nous avons ajouté des kits sur le site mobile. Lorsqu'on ajoute l'actualisation se fait directement. Nous avons tenté de connecter un utilisateur à partir de l'application.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous devons régler la connexion d'un utilisateur sur l'application mobile, récupérer les données d'un fichier CSV pour les rentrer dans la base de données et modifier la SPA.


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
La connexion d'un utilisateur, une SPA responsive et la notation des kits. 


### Qu'avons nous observé ? 
La SPA est bien responsive, la connexion ne marche pas contrairement à la notation des kits.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons régler le problème de connexion.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tester ce que nous avons cité précédemment.

### À quoi verra-t-on que celà à fonctionné ?
S'il n'y a pas d'erreurs, de bugs...

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
