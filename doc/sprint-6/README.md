# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons modfié le style de la SPA et nous avons créé des requêtes et inséré des données. Nous avons aussi réglé le problème de connexion du client sur l'application mobile.   

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons continuer de modifier le style de la SPA, implémenter la liste des produits sur l'application et régler le problème avec la table Kit sur le serveur.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nous avons testé l'affichage du style de la SPA et aussi l'affichage des données sur le serveur.

### Qu'avons nous observé ? 
Nous avons observé un problème avec la table kit.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons régler le problème avec la table kit.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons faire les choses citées précédemment.

### À quoi verra-t-on que celà à fonctionné ?
Si comme toujours aucun bug ne se présente, qu'on arrive à afficher le contenu de la table kit sur le web grâce au serveur et que la liste des produits s'affiche dans l'application.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
