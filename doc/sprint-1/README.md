# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Mise en route du serveur REST et de l'application Android

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons créer le site web, créer l'application, et modifier le serveur REST

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le serveur REST

### Qu'avons nous observé ? 
Il marche, il affiche "Hello World"

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons le modifier et essayer de le relier à l'application

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tester que l'application soit fonctionnelle ainsi que le serveur et le site web

### À quoi verra-t-on que celà à fonctionné ?
Si les trois sont reliés

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
