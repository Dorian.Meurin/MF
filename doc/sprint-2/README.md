# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons modifié le site web, et tentons de connecter l'application à Internet. La modification du serveur REST est toujours en cours. La base de données est aussi en cours de création.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons continuer de modifier le site web, l'application et le serveur REST

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le site web

### Qu'avons nous observé ? 
Nous avons remarqué qu'une base de données est créée dans notre dossier tmp

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons modifier le chemin

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tester que l'application soit fonctionnelle ainsi que le serveur et le site web

### À quoi verra-t-on que celà à fonctionné ?
Lorsqu'on pourra observer les stocks

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
