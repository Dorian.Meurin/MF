# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons réaliser des requêtes sql, modifier la SPA, le serveur et l'application mobile 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons continuer de faire les choses précédemment citées.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nous avons insérer des données dans la base de données

### Qu'avons nous observé ? 
Les données ont bien été insérées.

### Quelle décision prenons nous suite à cette expérience ? 
Comme dit précédemment, nous allons continuer de réaliser le tout.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons tester l'application, le serveur et la base de données.

### À quoi verra-t-on que celà à fonctionné ?
Si aucun bug ne se présente.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
