# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons encore modifié le site web (changement de la page en fonction de la fonctionnalité choisie), et nous avons réussi à connecter l'application à Internet. Les objets DAO ont été créés. La base de données est toujours en cours de création.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons connecter les objets DAO et la base de données. 

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
La connexion entre le serveur et le téléphone.

### Qu'avons nous observé ? 
"Hello World" s'affiche sur l'application mobile

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons maintenir cette connexion.

### Qu'allons nous tester durant les 2 prochaines heures ? 
La connexion entre l'application mobile et le serveur. 

### À quoi verra-t-on que celà à fonctionné ?
Cela fonctionne si on peut observer les stocks.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
