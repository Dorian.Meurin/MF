# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons affiché le contenu des kits sur l'application et les stocks, factures et produits sur le site web. Nous pouvons aussi ajouter un nouveau produit sur le site web qui sera ensuite enregistré dans la base de données. 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Afficher le contenu d'un kit et modifier des données sur l'application, afficher les détails des commandes sur le site web.


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'affichage des produits d'une commande et du contenu d'un kit, et la connexion d'un client sur l'application.

### Qu'avons nous observé ? 
Nous avons observé que le contenu ne veut plus s'afficher, mais la liste oui. 

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons modifier les programmes.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Les affichages sur le site web et sur l'application mobile.

### À quoi verra-t-on que celà à fonctionné ?
Si nous n'observons aucune anomalie.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
