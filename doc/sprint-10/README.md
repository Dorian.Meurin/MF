# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons créer un formulaire pour ajouter un produit (pas encore fonctionnel), nous avons réaliser les requêtes REST pour l'ajout des produits. Nous avons fait la mise en page de l'application mobile en affichant le logo et nous avons géré les clicks sur les kits. 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons rendre fonctionnel le formulaire afin que le nouveau produit entré puisse être enregistré dans la base. Nous allons vérifier s'il y a encore des requêtes REST à réaliser. Nous allons afficher les détails des kits lorsqu'on effectue un click. 

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'affichage du formulaire de saisie dans le site web et l'affichage du logo dans l'application.

### Qu'avons nous observé ? 
Les deux fonctionnent.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons enregistrer dans la base de données un nouvel utilisateur créé dans l'application mobile.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Le formulaire de saisie pour que ce qui est entré soit enregistré dans la base et de même pour un client sur l'application.

### À quoi verra-t-on que celà à fonctionné ?
Si ces données s'affichent dans la base alors cela a fonctionné.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
