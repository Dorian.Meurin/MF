# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons encore modfié le style de la SPA pour avoir un bel affichage de la liste de produits. Nous avons tenté d'afficher la liste des produits sur l'application mobile. 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons continuer de modifier le style de la SPA et créer les requêtes REST des autres tables.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nous avons testé l'affichage de la liste des produits sur le site web et l'application et nous avons réglé le problème avec la requête REST de la table Kit.

### Qu'avons nous observé ? 
La liste s'affiche bien sur le site et sur l'application.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons modifier le style.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons modifier le style de la SPA et de l'application et créer les requêtes REST des tables restantes.

### À quoi verra-t-on que celà à fonctionné ?
Si on obtient un beau design du site web et de l'application.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
