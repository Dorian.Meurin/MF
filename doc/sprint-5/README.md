# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons réalisé des requêtes sql, nous continuons à modifier la SPA, le serveur et l'application mobile. 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons parser les requêtes, .

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nous avons tenté de connecter un client sur l'application mobile et nous avons aussi testé le serveur. 

### Qu'avons nous observé ? 
Le serveur envoie les données sur le site web.

### Quelle décision prenons nous suite à cette expérience ? 
Comme dit précédemment, nous allons continuer de réaliser le tout.

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons développer la connexion d'un client sur l'application mobile et maintenir le serveur.

### À quoi verra-t-on que celà à fonctionné ?
Si aucun bug ne se présente, que le client arrive à se connecter et que le serveur continue à envoyer des données

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
