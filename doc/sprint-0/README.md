# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons discuté du projet avec le porteur de projet

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Mettre en place l'environnement (affichage du "Hello World")

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Rien

### Qu'avons nous observé ? 
Nous avons observé que nous n'avons pas eu la même compréhension du projet

### Quelle décision prenons nous suite à cette expérience ? 
Nous nous sommes répartis les tâches pour le prochain sprint

### Qu'allons nous tester durant les 2 prochaines heures ? 
Nous allons créer le site web, créer la base de données et créer l'application

### À quoi verra-t-on que celà à fonctionné ?
Si le site web s'affiche correctement, que la base de données comporte des produits et que l'application démarre

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
