# Rétrospective de sprint

Nom du scrum master du sprint : Mariama

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint: 
Nous avons changé le style de la SPA et réussi à afficher les produits. Nous avons réalisé la commande REST pour afficher les coffrets. Nous avons réussi à parser les données du serveur sur l'application mobile. Nous avons créé la page d'inscription sur l'application mobile.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint: 
Nous allons continuer d'améliorer le design et d'afficher les autres données, d'afficher le contenu d'un kit sur l'application, et faire en sorte qu'un client puisse s'inscrire.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'affichage des coffrets, l'affichage des kits sur l'application

### Qu'avons nous observé ? 
Cela fonctionne.

### Quelle décision prenons nous suite à cette expérience ? 
Nous allons améliorer l'affichage des données.

### Qu'allons nous tester durant les 2 prochaines heures ? 
L'inscription d'un client sur l'application, le style de la SPA.

### À quoi verra-t-on que celà à fonctionné ?
Dans l'application si l'on clique sur les données du kit on verra que cela a fonctionné. 

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
