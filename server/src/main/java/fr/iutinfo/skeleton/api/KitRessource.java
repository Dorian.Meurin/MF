package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/kit")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KitRessource {
	final static Logger logger = LoggerFactory.getLogger(KitRessource.class);
	private static KitDao dao = getDbi().open(KitDao.class);

	public void KitsResource() throws SQLException {
		if (!tableExist("kit")) {
			logger.debug("Create table kit");
			dao.createKitTable();

		}
	}

	@POST
	public Kit createKit(Kit kit) {

		int id_item = dao.insertItem(kit);
		kit.setId_kit(id_item);
		int id_kit = dao.insertKit(kit);

		return kit;
	}

	@GET
	@Path("/{id_kit}")
	public List<Kit> getKit(@PathParam("id_kit") String id_kit) {
		List<Kit> kit = dao.findByName(id_kit);
		if (kit == null) {
			throw new WebApplicationException(404);
		}
		return kit.stream().map(Kit::convertToDto).collect(Collectors.toList());
	}

	@GET
	public List<Kit> getAllKit(@QueryParam("q") String query) {
		List<Kit> kits;
		if (query == null) {
			kits = dao.all();
		} else {
			logger.debug("Search users with query: " + query);
			kits = dao.search("%" + query + "%");
		}
		return kits.stream().map(Kit::convertToDto).collect(Collectors.toList());
	}

	@GET
	@Path("/coffret")
	public List<Kit> get() {
		List<Kit> coffret = dao.coffret();

		if (coffret == null) {
			throw new WebApplicationException(404);
		}
		return coffret.stream().map(Kit::convertToDto).collect(Collectors.toList());
	}

	@GET
	@Path("/kit")
	public List<Kit> getkit() {
		List<Kit> coffret = dao.kit();

		if (coffret == null) {
			throw new WebApplicationException(404);
		}
		return coffret.stream().map(Kit::convertToDto).collect(Collectors.toList());
	}

	@PUT
	@Path("/like/{id_kit}")
	public void modifyLike(@PathParam("id_kit") int id_kit) {
		dao.modifyLike(id_kit);
	}

	@GET
	@Path("/{id_kit}/reaction/{id_personne}")
	public List<Kit> getReaction(@PathParam("id_kit") int id_kit, @PathParam("id_personne") int id_personne) {
		System.out.println(id_kit);
		System.out.println(id_personne);

		List<Kit> kits;
		kits = dao.getReaction(id_personne, id_kit);
		System.out.println(kits.size());
		return kits.stream().map(Kit::convertToDto).collect(Collectors.toList());
	}

	@PUT
	@Path("/dislike/{id_kit}")
	public void modifyDislike(@PathParam("id_kit") int id_kit) {
		dao.modifyDislike(id_kit);
	}

	@DELETE
	@Path("/{id_kit}")
	public void deleteUser(@PathParam("id_kit") int id_kit) {
		dao.delete(id_kit);
	}


	@PUT
	@Path("/quantite")
	public void modifyLike(Kit kit) {
		dao.modifyQuantite(kit);
	}

	@POST
	@Path("/composant")
	public void addInKit(Kit kit) {
		dao.insertInKit(kit);
	}

}
