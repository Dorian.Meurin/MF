package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Commande {
    final static Logger logger = LoggerFactory.getLogger(Commande.class);
    private Integer id_commande;
    private Integer id_client;
    private String nom_personne ; 
    private String prenom_personne; 
    private Integer qte ; 
    public Integer getQte() {
		return qte;
	}
	public void setQte(Integer qte) {
		this.qte = qte;
	}
	public Commande(Integer qte, String libelle, String type) {
		super();
		this.qte = qte;
		this.libelle = libelle;
		this.type = type;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}


	private String libelle ; 
    private String type ;
    public String getPrenom_personne() {
		return prenom_personne;
	}
	public void setPrenom_personne(String prenom_personne) {
		this.prenom_personne = prenom_personne;
	}
	public String getEmail_personne() {
		return email_personne;
	}
	public void setEmail_personne(String email_personne) {
		this.email_personne = email_personne;
	}


	private String email_personne; 

    


	public String getNom_personne() {
		return nom_personne;
	}
	public void setNom_personne(String nom_personne) {
		this.nom_personne = nom_personne;
	}
	public Commande(Integer id_commande, Integer id_client) {
		super();
		this.id_commande = id_commande;
		this.id_client = id_client;
	}
	public Commande(Integer id_commande, Integer id_client, String nom_personne, String prenom_personne , String email_personne) {
		super();
		this.id_commande = id_commande;
		this.id_client = id_client;
		this.prenom_personne = prenom_personne; 
		this.email_personne = email_personne ; 
	}

	

	public Commande() {
    }

  
  

	public Integer getId_commande() {
		return id_commande;
	}


	public void setId_commande(Integer id_commande) {
		this.id_commande = id_commande;
	}


	public Integer getId_client() {
		return id_client;
	}


	public void setId_client(Integer id_client) {
		this.id_client = id_client;
	}


	public static Logger getLogger() {
		return logger;
	}


	 public Commande convertToDto() {
	        return this;
	    }

  
  
}
