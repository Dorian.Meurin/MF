package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Item {
    final static Logger logger = LoggerFactory.getLogger(Item.class);
    private Integer id;
    private String type;
    private String libelle;
    public String getLibelle() {
		return libelle;
	}

	public Item(Integer id, String type, String libelle) {
		super();
		this.id = id;
		this.type = type;
		this.libelle = libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Item(Integer id, String type) {
		super();
		this.id = id;
		this.type=type;
	}

	public Item() {
    }

  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	@Override
	public String toString() {
		return "Item [id=" + id + ", type=" + type + "]";
	}

	public static Logger getLogger() {
		return logger;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	 public Item convertToDto() {
	        return this;
	    }
}
