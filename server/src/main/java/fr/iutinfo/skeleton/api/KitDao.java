package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

//A modifier
public interface KitDao {
    @SqlUpdate("CREATE TABLE kit( \n" + 
    		"	id_kit integer not null, \n" + 
    		"	id_personne integer not null,\n" + 
    		"	CONSTRAINT id_kit_unique UNIQUE (id_kit),\n" + 
    		"	FOREIGN key( id_personne) REFERENCES personne(id)\n" + 
    		"); ")
    void createKitTable();

    @SqlUpdate("insert into item (libelle, type) values (:libelle, 'KIT')")
    @GetGeneratedKeys
    int insertItem(@BindBean() Kit kit);
    
    @SqlUpdate("insert into kit (id_kit,id_personne) values (:id_kit, :id_personne)")

    @GetGeneratedKeys
    int insertKit(@BindBean() Kit kit);

    @SqlQuery("select i.libelle, k.quantite,k.id_produit,k.id_kit  from kitComposant as k,produit as p , item as i  where k.id_produit = p.id_produit AND k.id_produit = i.id_item AND k.id_kit = :id_kit")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> findByName(@Bind("id_kit") String id_kit);

    @SqlQuery("select * from kit where search like :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> search(@Bind("id") String id);



    @SqlUpdate("delete from kit where id_kit = :id_kit")
    void delete(@Bind("id_kit") int id_kit);

    @SqlQuery("select * from kit")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> all();

    @SqlQuery("select * from kit where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Kit findById(@Bind("id") int id);
    
    @SqlQuery("select *  from kit as k, item as i where k.id_kit =i.id_item AND i.type = 'COFFRET' ")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> coffret();
    
    @SqlQuery("select *  from kit as k, item as i where k.id_kit =i.id_item AND i.type = 'KIT' ")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> kit();


    @SqlUpdate("update kit set kit_like=kit_like+1 where id_kit = :id_kit ")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int  modifyLike(@Bind("id_kit") int id_kit);
    
    @SqlUpdate("update kit set kit_dislike=kit_dislike+1 where id_kit = :id_kit ")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int  modifyDislike(@Bind("id_kit") int id_kit);
 
    @SqlQuery("select * from kitcote where id_personne = :id_personne AND id_kit = :id_kit")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Kit> getReaction(@Bind("id_personne") int id_personne, @Bind("id_kit") int id_kit);
    
    

    @SqlUpdate("update kitcomposant set quantite = :quantite where id_kit = :id_kit and id_produit = :id_produit")
    @RegisterMapperFactory(BeanMapperFactory.class)
	void modifyQuantite(@BindBean()Kit kit);

    
    @SqlUpdate("insert into kitComposant (id_kit, id_produit) values (:id_kit, :id_produit)")
    @GetGeneratedKeys
	int insertInKit(@BindBean()Kit kit);
    
    void close();

}
