package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Personne {
    final static Logger logger = LoggerFactory.getLogger(Personne.class);
    private Integer id_personne;
    private String nom_personne;
    private String prenom_personne;
    private String email_personne;
    private String passwdHash;
    private String societe;
    private String role_personne;
    

	

	public Personne() {
    }
	
	

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}

	



	public Personne(Integer id_personne, String nom_personne, String prenom_personne, String email_personne,
			String passwdHash, String societe, String role_personne) {
		super();
		this.id_personne = id_personne;
		this.nom_personne = nom_personne;
		this.prenom_personne = prenom_personne;
		this.email_personne = email_personne;
		this.passwdHash = passwdHash;
		this.societe = societe;
		this.role_personne = role_personne;
	}



	public Integer getId_personne() {
		return id_personne;
	}



	public void setId_personne(Integer id_personne) {
		this.id_personne = id_personne;
	}



	public String getNom_personne() {
		return nom_personne;
	}



	public void setNom_personne(String nom_personne) {
		this.nom_personne = nom_personne;
	}



	public String getPrenom_personne() {
		return prenom_personne;
	}



	public void setPrenom_personne(String prenom_personne) {
		this.prenom_personne = prenom_personne;
	}



	public String getEmail_personne() {
		return email_personne;
	}



	public void setEmail_personne(String email_personne) {
		this.email_personne = email_personne;
	}



	public String getPasswdHash() {
		return passwdHash;
	}



	public void setPasswdHash(String passwdHash) {
		this.passwdHash = passwdHash;
	}



	public String getRole_personne() {
		return role_personne;
	}



	public void setRole_personne(String role_personne) {
		this.role_personne = role_personne;
	}



	public static Logger getLogger() {
		return logger;
	}



	 public Personne convertToDto() {
	        return this;
	    }

  
  
}
