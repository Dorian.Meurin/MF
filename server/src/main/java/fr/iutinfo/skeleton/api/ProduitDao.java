package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface ProduitDao {
    @SqlUpdate("CREATE TABLE produit(\n" + 
    		"	id_produit integer not null  , \n" + 
    		"	id_fournisseur integer not null, \n" + 
    		"	libelle text not null, \n" + 
    		"	qteParArticle integer not null DEFAULT(1),\n" + 
    		"	stock integer not null, \n" + 
    		"	poidPackaging integer not null, \n" + 
    		"	prixPackaging integer not null, \n" + 
    		"	refFourniseurArticle text not null, \n" + 
    		"	CONSTRAINT id_produit_unique UNIQUE (id_produit),\n" + 
    		"	FOREIGN KEY(id_fournisseur) REFERENCES fournisseur(id_personne), \n" + 
    		"	FOREIGN KEY(id_produit) REFERENCES item(id_item)\n" + 
    		");")
    void createProduitTable();

    @SqlUpdate("insert into item (libelle, type) values (:libelle, 'PRODUIT')")
    @GetGeneratedKeys
    int insertItem(@BindBean() Produit d );
    
    @SqlUpdate("INSERT INTO produit(id_produit, qteParArticle, stock, poidPackaging, prixPackaging, refFourniseurArticle ) VALUES(:id_produit, :qteParArticle, :stock, :poidPackaging, :prixPackaging, :refFourniseurArticle  ) ")

    @RegisterMapperFactory(BeanMapperFactory.class)
	int insertProduit(@BindBean() Produit dto);
    


    @SqlQuery("select * from produits where name = :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Produit findByName(@Bind("name") String name);

    @SqlQuery("select * from produits where search like :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Produit> search(@Bind("name") String name);

    @SqlUpdate("drop table if exists produits")
    void dropProduitTable();

    @SqlUpdate("delete from produit where id_produit = :id")
    void deleteProduit(@Bind("id") int id);

    @SqlUpdate("delete from item where id_item = :id")
    void deleteItem(@Bind("id") int id);

    @SqlQuery("select * from produit as p, item as i WHERE p.id_produit = i.id_item ")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Produit> all();

    @SqlQuery("select * from produits where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Produit findById(@Bind("id") int id);

    void close();

    @SqlUpdate("update produit set stock = :number where id_produit = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int  modifyStock(@Bind("id") int id, @Bind("number") int number);
    

}
