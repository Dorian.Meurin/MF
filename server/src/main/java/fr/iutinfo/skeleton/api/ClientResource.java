package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/client")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClientResource {
    final static Logger logger = LoggerFactory.getLogger(ClientResource.class);
    private static ClientDao dao = getDbi().open(ClientDao.class);

    public ClientResource() throws SQLException {
        if (!tableExist("client")) {
            logger.debug("Create table users");
            dao.createUserTable();
            dao.insert(new Client());
        }
    }

    @POST
    public void createClient(Client c ) {

    	dao.insert(c);
    	
    	
    }

    @GET
    @Path("/{name}")
    public UserDto getUser(@PathParam("name") String name) {
        User user = dao.findByName(name);
        if (user == null) {
            throw new WebApplicationException(404);
        }
        return user.convertToDto();
    }

    @GET
    public List<Client> getAllUsers(@QueryParam("q") String query) {
        List<Client> client = null;
        if (query == null) {
            client = dao.all();
        } else {
            logger.debug("Search users with query: " + query);
           
        }
        return client.stream().map(Client::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") int id) {
        dao.delete(id);
    }

}
