package fr.iutinfo.skeleton.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/personne")
//todo
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonneRessource {
    final static Logger logger = LoggerFactory.getLogger(PersonneRessource.class);
    private static PersonneDao dao = getDbi().open(PersonneDao.class);

    public PersonneRessource() throws SQLException {
        if (!tableExist("Personne")) {
            logger.debug("Create table adresse");
            dao.createUserTable();
            
        }
        else
        {
        	System.out.println("Table déja existante");
        }
    }

    @POST
    public Personne createPersonne(Personne ad) {
    	
    if(dao.emailExist(ad.getEmail_personne())< 1)
    {
        int id = dao.insert(ad);
        System.out.println(id);
    }
    else
    {
    	return null;
    }



        return ad;
    }

    @GET
    @Path("/email/{email_personne}")
    public Personne getAdresse(@PathParam("email_personne") String email_personne) {
    	Personne personne = dao.findByEmail(email_personne);
        if (personne == null) {
        	personne = null;
            throw new WebApplicationException(404);
        }
        return personne;
    }

    
    
    @GET
    @Path("/{id}")
    public Personne getByID(@PathParam("id") int id_personne) {
    	Personne  p = dao.findById(id_personne);
    	
    	return p ;  
    }
    
 

    @GET
    public List<Personne> getAllPersonne(@QueryParam("q") String query) {
        List<Personne> personne;
        if (query == null) {
            personne = dao.all();
        } else {
            logger.debug("Search Adresse with query: " + query);
            personne = dao.search("%" + query + "%");
        }
        return personne.stream().map(Personne::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") int id) {
        dao.delete(id);
    }

}
