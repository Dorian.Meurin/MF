package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface ClientDao {
    @SqlUpdate("CREATE TABLE client (\n" + 
    		"	id_personne integer not null ,\n" + 
    		"	id_adrsFacturation,\n" + 
    		"	id_adrsLivraison,\n" + 
    		"	FOREIGN KEY(id_personne) REFERENCES Personne(id_personne), \n" + 
    		"	FOREIGN KEY(id_adrsFacturation) REFERENCES adress(id_adresse), \n" + 
    		"	FOREIGN KEY(id_adrsLivraison) REFERENCES adress(id_adresse)\n" + 
    		");")
    void createUserTable();

    @SqlUpdate("insert into client (id_personne,id_adrsFacturation,id_adrsLivraison) values (:id_personne, :id_adrsFacturation, :id_adrsFacturation")
    @GetGeneratedKeys
    int insert(@BindBean() Client client);

    @SqlQuery("select * from users where name = :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findByName(@Bind("name") String name);




    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);

    @SqlQuery("select * from client order by id_personne")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Client> all();

    @SqlQuery("select * from users where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findById(@Bind("id") int id);

    void close();
}
