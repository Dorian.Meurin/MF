package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/adresse")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdressRessource {
    final static Logger logger = LoggerFactory.getLogger(AdressRessource.class);
    private static AdresseDao dao = getDbi().open(AdresseDao.class);

    public AdressRessource() throws SQLException {
        if (!tableExist("adresse")) {
            logger.debug("Create table adresse");
            dao.createUserTable();
            dao.insert(new Adresse(null, "Margaret Thatcher", "la Dame de fer", null, null, null));
        }
        else
        {
        	System.out.println("Table déja existante");
        }
    }

    @POST
    public Adresse createAdresse(Adresse ad) {
    	
    
        int id = dao.insert(ad);


        return ad;
    }

    @GET
    @Path("/{id}")
    public Adresse getAdresse(@PathParam("id") String id) {
        Adresse ad = dao.findByName(id);
        if (ad == null) {
            throw new WebApplicationException(404);
        }
        return ad.convertToDto();
    }

    @GET
    public List<Adresse> getAllAdress(@QueryParam("q") String query) {
        List<Adresse> Adress;
        if (query == null) {
            Adress = dao.all();
        } else {
            logger.debug("Search Adresse with query: " + query);
            Adress = dao.search("%" + query + "%");
        }
        return Adress.stream().map(Adresse::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") int id) {
        dao.delete(id);
    }

}
