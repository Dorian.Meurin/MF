package fr.iutinfo.skeleton.api;


public class Produit {
    private int id_produit;
    private int id_fournisseur;
    private String libelle ; 
	private int qteParArticle;
    private int stock;
    @Override
	public String toString() {
		return "Produit [id_produit=" + id_produit + ", id_fournisseur=" + id_fournisseur + ", libelle=" + libelle
				+ ", qteParArticle=" + qteParArticle + ", stock=" + stock + ", poidPackaging=" + poidPackaging
				+ ", prixPackaging=" + prixPackaging + ", refFourniseurArticle=" + refFourniseurArticle + "]";
	}

	private double poidPackaging;
    private double prixPackaging;
    private String refFourniseurArticle;

    public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

    public int getId_produit() {
		return id_produit;
	}

	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}

	public int getId_fournisseur() {
		return id_fournisseur;
	}

	public void setId_fournisseur(int id_fournisseur) {
		this.id_fournisseur = id_fournisseur;
	}

	

	public int getQteParArticle() {
		return qteParArticle;
	}

	public void setQteParArticle(int qteParArticle) {
		this.qteParArticle = qteParArticle;
	}

	



    public Produit() {
    }

		public Produit( String libelle, int id_produit, int id_fournisseur, int qteParArticle, int stock, double poidPackaging,
			double prixPackaging, String refFourniseurArticle ) {
		super();
		this.id_produit = id_produit;
		this.id_fournisseur = id_fournisseur;
		this.qteParArticle = qteParArticle;
		this.stock = stock;
		this.libelle = libelle ; 
		this.poidPackaging = poidPackaging;
		this.prixPackaging = prixPackaging;
		this.refFourniseurArticle = refFourniseurArticle;
	}

		public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPoidPackaging() {
		return poidPackaging;
	}

	public void setPoidPackaging(double poidPackaging) {
		this.poidPackaging = poidPackaging;
	}

	public double getPrixPackaging() {
		return prixPackaging;
	}
 
    

	public void setPrixPackaging(double prixPackaging) {
		this.prixPackaging = prixPackaging;
	}



		public String getRefFourniseurArticle() {
		return refFourniseurArticle;
	}

	public void setRefFourniseurArticle(String refFourniseurArticle) {
		this.refFourniseurArticle = refFourniseurArticle;
	}

		public Produit convertToDto() {
	   
	
	       
	        return this;
	    }
    
    
}
