package fr.iutinfo.skeleton.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;



@Path("/produit")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProduitRessource {
    final static Logger logger = LoggerFactory.getLogger(ProduitRessource.class);
    private static ProduitDao dao = getDbi().open(ProduitDao.class);

    public ProduitRessource() throws SQLException {
        if (!tableExist("produit")) {
            logger.debug("Create table produits");
            dao.createProduitTable();

        }
    }
    @GET
    public List<Produit> getAllProduit(@QueryParam("q") String query) {
        List<Produit> produit;
        if (query == null) {
            produit = dao.all();
        } else {
            logger.debug("Search users with query: " + query);
            produit = dao.search("%" + query + "%");
        }
        System.out.println(produit.get(0).toString());
        return produit.stream().map(Produit::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Produit createProduit(Produit dto) {
    	int id_item = dao.insertItem(dto);
    	dto.setId_produit(id_item);
    	int id_produit = dao.insertProduit(dto);
        return dto;
    }

    
    
   /* @PUT
    @Path("/stock/{id}/{number}")
    public void modifyStockProduit(@PathParam("id") int id, @PathParam("number") int number) {
    	dao.modifyStock(id,number);
    }
   
    */
    @PUT
    @Path("/stock")
    public void modifyStockProduit(Produit dto) {
    	dao.modifyStock(dto.getId_produit(),dto.getStock());}


    @DELETE
    @Path("/{id}")
    public void deleteProduit(@PathParam("id") int id) {

        dao.deleteProduit(id);
    

    }

 
    

}
