package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface CommandeDao {
    @SqlUpdate("CREATE TABLE adresse(\n" + 

    		");")
    void createCommandeTable();

    
    @SqlUpdate("insert into commande(id_client) values (:id_client)")
    @GetGeneratedKeys
    int insert(@BindBean() Commande ad);

    @SqlQuery("select * from adresse where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Commande findByName(@Bind("id") String id);

    @SqlQuery("select * from users where search like :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Commande> search(@Bind("name") String name);

    @SqlUpdate("drop table if exists users")
    void dropUserTable();

    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);

    @SqlQuery("select * from personne ,commande  where personne.id_personne = commande.id_client ")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Commande> all();

    @SqlQuery("SELECT d.qte, i.libelle, i.type  FROM detailcommande as d, item as i  WHERE d.id_item = i.id_item AND d.id_cmd = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Commande> findById(@Bind("id") int id);

    void close();

	
}
