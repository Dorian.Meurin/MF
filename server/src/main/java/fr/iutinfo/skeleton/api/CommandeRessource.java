package fr.iutinfo.skeleton.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/commande")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommandeRessource {
    final static Logger logger = LoggerFactory.getLogger(Commande.class);
    private static CommandeDao dao = getDbi().open(CommandeDao.class);

    public CommandeRessource() throws SQLException {
        if (!tableExist("commande")) {
            logger.debug("Create table commande");
            dao.createCommandeTable();
        }
        else
        {
        	System.out.println("Table déja existante");
        }
    }

    @POST
    public Commande createAdresse(Commande ad) {
    	
        int id = dao.insert(ad);
        return ad;
    }

    @GET
    @Path("/{id}")
    public List<Commande> getCommande(@PathParam("id") int id) {
        List<Commande> ad = dao.findById(id);
        if (ad == null) {
            throw new WebApplicationException(404);
        }
        return ad ; 
    }

    @GET
    public List<Commande> getAllAdress(@QueryParam("q") String query) {
        List<Commande> comande;
        if (query == null) {
            comande = dao.all();
        } else {
            logger.debug("Search Adresse with query: " + query);
            comande = dao.search("%" + query + "%");
        }
        return comande.stream().map(Commande::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") int id) {
        dao.delete(id);
    }

}
