package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface PersonneDao {
    @SqlUpdate("CREATE TABLE Personne(\n" + 
    		"	id_personne integer  primary key AUTOINCREMENT, \n" + 
    		"	nom_personne varchar(100) not null, \n" + 
    		"	prenom_personne varchar(100) not null, \n" + 
    		"	societe varchar(255),\n" + 
    		"	role_personne text DEFAULT('CREATEUR') CHECK (role_personne in ('ADMIN','CREATEUR')) 	\n" + 
    		");")
    void createUserTable();

    // select count(*) from personne where email=  :email;  
    @SqlUpdate("INSERT INTO personne(nom_personne,prenom_personne,email_personne,passwdHash,societe,role_personne) VALUES (:nom_personne,:prenom_personne,:email_personne,:passwdHash,:societe,:role_personne)")
    @GetGeneratedKeys
    int insert(@BindBean() Personne ad);

    @SqlQuery("select * from personne where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Personne findByName(@Bind("id") String id);
    
    @SqlQuery("select count(*) from personne where email_personne = :email_personne")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Integer emailExist(@Bind("email_personne") String email_personne);

    @SqlQuery("select * from personne where email_personne = :email_personne")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Personne findByEmail(@Bind("email_personne") String email_personne);


    @SqlQuery("select * from users where search like :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Personne> search(@Bind("name") String name);



    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);

    @SqlQuery("select * from personne")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Personne> all();

    @SqlQuery("select * from personne where id_personne = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Personne findById(@Bind("id") int id);

    void close();

	
}
