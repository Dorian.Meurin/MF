package fr.iutinfo.skeleton.api;


import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//todo
public class Adresse {
    final static Logger logger = LoggerFactory.getLogger(Adresse.class);
   // private static Adresse anonymous = new Adresse(-1, "Anonymous", "anonym", 12, "", "");
    private Integer id_adresse;
    private String ville;
    private String rue;
    private Integer numero;
    private String complement;
    private String tel;
    private String codePostal;
    private String pays;
    
    public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

    



    public Adresse(Integer id_adresse, String ville, String rue, Integer numero, String codePostal, String pays) {
		super();
		this.id_adresse= id_adresse;
		this.ville = ville;
		this.rue = rue;
		this.numero = numero;
		this.codePostal = codePostal;
		this.pays = pays;
	}

	public Adresse() {
    }

  

    

	public Integer getId_adresse() {
		return id_adresse;
	}

	public void setId_adresse(Integer id_adresse) {
		this.id_adresse = id_adresse;
	}

	@Override
	public String toString() {
		return "Adresse [id=" + id_adresse + ", ville=" + ville + ", rue=" + rue + ", numero=" + numero + ", codePostal="
				+ codePostal + ", pays=" + pays + "]";
	}


	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public static Logger getLogger() {
		return logger;
	}

	public void setId(Integer id_adresse) {
		this.id_adresse = id_adresse;
	}

	 public Adresse convertToDto() {
	       
	      
	        return this;
	    }

  
  
}
