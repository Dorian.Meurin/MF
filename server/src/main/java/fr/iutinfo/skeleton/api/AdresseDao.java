package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface AdresseDao {
    @SqlUpdate("CREATE TABLE adresse(\n" + 
    		"	id_adresse SERIAL primary key autoincrement, \n" + 
    		"	numero integer not null, \n" + 
    		"	rue varchar(255) not null, \n" + 
    		"	ville varchar(255) not null,  \n" + 
    		"	complement text , \n" + 
    		"	codePostal varchar(255)not null, \n" + 
    		"	pays varchar(255) not null,\n" + 
    		"	tel varchar(12) not null\n" + 
    		");")
    void createUserTable();

    
    @SqlUpdate("insert into adresse (numero,rue, ville, complement,codePostal,pays,tel) values (:numero, :rue, :ville, :complement, :codePostal,:pays,:tel)")
    @GetGeneratedKeys
    int insert(@BindBean() Adresse ad);

    @SqlQuery("select * from adresse where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Adresse findByName(@Bind("id") String id);

    @SqlQuery("select * from users where search like :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Adresse> search(@Bind("name") String name);

    @SqlUpdate("drop table if exists users")
    void dropUserTable();

    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);

    @SqlQuery("select * from adresse")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Adresse> all();

    @SqlQuery("select * from users where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findById(@Bind("id") int id);

    void close();

	
}
