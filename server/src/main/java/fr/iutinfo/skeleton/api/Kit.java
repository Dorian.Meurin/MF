package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kit {
//post
    final static Logger logger = LoggerFactory.getLogger(Kit.class);

    private Integer id_produit; // sers quand on liste le kit
	public Kit(Integer id_produit, Integer id_kit, Integer id_personne, Integer kit_like, Integer kit_dislike,
			String libelle, String type, Integer quantite) {
		super();
		this.id_produit = id_produit;
		this.id_kit = id_kit;
		this.id_personne = id_personne;
		this.kit_like = kit_like;
		this.kit_dislike = kit_dislike;
		this.libelle = libelle;
		this.type = type;
		this.quantite = quantite;
	}

	public Integer getId_produit() {
		return id_produit;
	}

	public void setId_produit(Integer id_produit) {
		this.id_produit = id_produit;
	}
	private Integer id_kit;
	private Integer id_personne;
	private Integer kit_like ; 
	
	
	public Kit(Integer id_kit, Integer id_personne, Integer kit_like, Integer kit_dislike, String libelle, String type,
			Integer quantite) {
		super();
		this.id_kit = id_kit;
		this.id_personne = id_personne;
		this.kit_like = kit_like;
		this.kit_dislike = kit_dislike;
		this.libelle = libelle;
		this.type = type;
		this.quantite = quantite;
	}

	public Integer getKit_like() {
		return kit_like;
	}

	public void setKit_like(Integer kit_like) {
		this.kit_like = kit_like;
	}
	private Integer kit_dislike ; 
	private String libelle ; 
	private String type ; 
	
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Kit(String libelle, Integer quantite) {
		super();
		this.libelle = libelle;
		this.quantite = quantite;
	}
	private Integer quantite;
	
	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public Kit() {
		
	}
	public Kit(Integer id_personne, String libelle) {
		this.id_personne = id_personne;
		this.libelle = libelle ;
	}
	public Kit(Integer id_kit, Integer id_personne, String libelle) {
		this.id_kit = id_kit;
		this.id_personne = id_personne;
		this.libelle = libelle ;
	}
			
		public Kit(Integer id_kit, Integer id_personne, String libelle, String type) {
		this.id_kit = id_kit;
		this.id_personne = id_personne;
		this.libelle = libelle ;
		this.type = type ; 
		
	}
	public Integer getId_kit() {
		return id_kit;
	}
	public void setId_kit(Integer id_kit) {
		this.id_kit = id_kit;
	}
	public Integer getId_personne() {
		return id_personne;
	}
	public void setId_personne(Integer id_personne) {
		this.id_personne = id_personne;
	}

	
	

	
	@Override
	public String toString() {
		return "Kit [id_kit=" + id_kit + ", id_personne=" + id_personne + ", quantite=" + quantite + "]";
	}

	public Kit convertToDto() {
		// TODO Auto-generated method stub
		return this;
	}
	public static Logger getLogger() {
		return logger;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getKit_dislike() {
		return kit_dislike;
	}

	public void setKit_dislike(Integer kit_dislike) {
		this.kit_dislike = kit_dislike;
	}


	

    
}
