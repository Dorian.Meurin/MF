package fr.iutinfo.skeleton.api;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//a modifier
public class Client {


	private Integer id_personne;
    private Integer id_adrsFacturation;
    private Integer id_adrsLivraison;

    
    public Client() {
		super();
	}





	public Client(Integer id_personne, Integer id_adrsFacturation, Integer id_adrsLivraison) {
		super();
		this.id_personne = id_personne;
		this.id_adrsFacturation = id_adrsFacturation;
		this.id_adrsLivraison = id_adrsLivraison;
	}





	final static Logger logger = LoggerFactory.getLogger(User.class);
   
    public Integer getId_personne() {
		return id_personne;
	}





	public void setId_personne(Integer id_personne) {
		this.id_personne = id_personne;
	}





	public Integer getId_adrsFacturation() {
		return id_adrsFacturation;
	}





	public void setId_adrsFacturation(Integer id_adrsFacturation) {
		this.id_adrsFacturation = id_adrsFacturation;
	}







 

	

	public static Logger getLogger() {
		return logger;
	}

	public Client convertToDto() {
		   
		
	       
        return this;
    }





	public Integer getId_adrsLivraison() {
		return id_adrsLivraison;
	}





	public void setId_adrsLivraison(Integer id_adrsLivraison) {
		this.id_adrsLivraison = id_adrsLivraison;
	}
	
}
