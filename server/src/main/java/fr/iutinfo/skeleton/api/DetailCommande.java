package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DetailCommande {
    final static Logger logger = LoggerFactory.getLogger(DetailCommande.class);
    private Integer idCommande;
    private Integer idClient;

	public DetailCommande(Integer id, Integer idClient) {
		super();
		this.idCommande = id;
		this.idClient=idClient;
	}

	public DetailCommande() {
    }

  
    public int getId() {
        return idCommande;
    }

    public void setId(int id) {
        this.idCommande = id;
    }
    
	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public void setId(Integer id) {
		this.idCommande = id;
	}

	@Override
	public String toString() {
		return "Item [id=" + idCommande + ", id client=" + idClient + "]";
	}

	public static Logger getLogger() {
		return logger;
	}


	 public DetailCommande convertToDto() {
	        return this;
	    }

  
  
}
