package fr.univ_lille1.iutinfo.mfapplication;

import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by pierrut on 23/03/18.
 */

public class KitViewHolder {
    public ImageView avatar;
    public TextView idPersonne;
    public TextView nomKit;

}
