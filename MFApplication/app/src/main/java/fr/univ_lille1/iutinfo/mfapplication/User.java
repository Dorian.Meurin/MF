package fr.univ_lille1.iutinfo.mfapplication;

/**
 * Created by boudonm on 23/03/18.
 */

public class  User {
    static public String nom;
    static public String prenom;
    static public String email;
    static public String psw;


    public String getNom(){
        return nom;
    }

    public String getPrenom(){
        return prenom;
    }

    public String getEmail(){
        return email;
    }

    public String getPsw(){
        return psw;
    }

    public void setPsw(String s){
        psw=s;
    }

    public void setNom(String nom){
        this.nom=nom;
    }

    public void setPrenom(String prenom){
        this.prenom=prenom;
    }

    public void setEmail(String email){
        this.email=email;
    }
}
