package fr.univ_lille1.iutinfo.mfapplication;

/**
 * Created by delsautj on 26/03/18.
 */

public class Produit {
    int id_produit;
    String libelle;

    public Produit(int id_produit,String libelle) {
        this.id_produit = id_produit;
        this.libelle=libelle;
    }

    @Override
    public String toString() {
        return "Produit : id_produit=" + id_produit +", libelle='" + libelle + '\'' ;
    }
}
