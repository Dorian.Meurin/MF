package fr.univ_lille1.iutinfo.mfapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static fr.univ_lille1.iutinfo.mfapplication.User.nom;
import static fr.univ_lille1.iutinfo.mfapplication.User.prenom;


public class AffichKit extends AppCompatActivity {

    public String chaineJson = "base";
    public String urlServ = "http://172.18.49.20:8080";
    public ListView mListView;
    private ArrayList<Produit> l = new ArrayList<>();
    private Kit kit;
    private boolean pressed = false;
    private boolean like = false;
    private boolean dislike = false;

    RequestQueue queue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affich_kit);

        Button monButton=(Button)findViewById(R.id.buttonLike);
        monButton.setBackgroundColor(0x7766CDAA);
        Button monButton1=(Button)findViewById(R.id.buttonDislike);
        monButton1.setBackgroundColor(0x77F08080);

        Button monButton2=(Button)findViewById(R.id.buttonDetails);
        monButton2.setBackgroundColor(0x77FFC0CB);

        TextView nom = (TextView) findViewById(R.id.idPersonneKit);
        nom.setTextColor(0x66FF1493);

        TextView title = (TextView) findViewById(R.id.libelleKit);
        title.setTextColor(0x66FF1493);



        TextView tvLibelle = (TextView) findViewById(R.id.libelleKit);
        TextView tvIDPersonne = (TextView) findViewById(R.id.idPersonneKit);
        queue = Volley.newRequestQueue(this);

        Intent intent = getIntent();
        int idPersonne = intent.getIntExtra("personne",0);
        String libelle = intent.getStringExtra("libelle");
        int idKit = intent.getIntExtra("id",0);
        Log.e("nomPersonne/libelle/Kit",idPersonne+" / "+libelle+"/"+idKit);
        String p = idPersonne+" ";
        tvIDPersonne.setText(prenom+" "+ nom);
        tvLibelle.setText(libelle);
        kit = new Kit(idKit,idPersonne,libelle);
        mListView = (ListView) findViewById(R.id.listKit);

        getJSON();
    }

    public String getJSON(){
        final String[] resultat = new String[]{""};

        final String url = urlServ+"/v1/kit/"+kit.id_kit; //10.0.2.2  172.18.49.19
        Response.Listener<String> getListReq = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ResponseJSON", response);
                resultat[0] = response;
                chaineJson = response;
            }
        };

        Response.ErrorListener errorGetReq = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String response= "Request GET ERROR" + error.getCause();
                Log.d("Error.Response", response);
                resultat[0] = response;
                chaineJson = response;
            }
        };

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, getListReq,errorGetReq);

        // add it to the RequestQueue
        queue.add(getRequest);
        return resultat[0];
    }

    public void onClickDetails(View view){
        if(!pressed) {
            pressed = true;
            JSONArray js = null;
            try {
                js = new JSONArray(chaineJson);
                Log.d("Affichage de l'id", js.getJSONObject(1).toString());
            } catch (JSONException e) {
                Log.d("Erreur de jsonexception", e.getMessage());
                e.printStackTrace();
            }
            if(js.length() == 0){
                Log.e("Champ vide ","Le kit est vide");
                TextView tvVide = (TextView) findViewById(R.id.tvVide);
                tvVide.setText("Le kit est vide");
            }
            for (int i = 0; i <= js.length(); i++) {
                JSONObject jObject = null;
                try {
                    jObject = js.getJSONObject(i);
                    l.add(new Produit(jObject.getInt("quantite"), jObject.getString("libelle")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            final ArrayAdapter<Produit> adapter = new ArrayAdapter<Produit>(AffichKit.this,
                    android.R.layout.simple_list_item_1, l);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

    }

    public void onLike(View view){
        if(!like) {
            like = true;

            Map<String, String> post = new HashMap<>();
            // post.put("id_kit",kit.id_kit+"");

            String url = urlServ + "/v1/kit/like/" + kit.id_kit;
            final TextView testErreur = (TextView) findViewById(R.id.testErreur);

            JsonObjectRequest json = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(post), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //  Toast.makeText(Requete.this, "ça marche l'insertion de user", Toast.LENGTH_SHORT).show();
                    //testErreur.setText("Reponse positive");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Toast.makeText(AffichKit.this, "ça marche pas insert user", Toast.LENGTH_LONG).show();
                    //testErreur.setText("Reponse erreur");
                }
            });
            queue.add(json);
        }
    }

    public void onDislike(View view){
        if(!dislike) {
            dislike = true;
            Map<String, String> post = new HashMap<>();
            // post.put("id_kit",kit.id_kit+"");

            String url = urlServ + "/v1/kit/dislike/" + kit.id_kit;
            final TextView testErreur = (TextView) findViewById(R.id.testErreur);

            JsonObjectRequest json = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(post), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //  Toast.makeText(Requete.this, "ça marche l'insertion de user", Toast.LENGTH_SHORT).show();
                    //testErreur.setText("Reponse positive");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Toast.makeText(AffichKit.this, "ça marche pas insert user", Toast.LENGTH_LONG).show();
                    //testErreur.setText("Reponse erreur");
                }
            });
            queue.add(json);
        }
    }

    private class Produit{
        int quantite;
        String libelle;
        Produit(int quantite, String libelle){
            this.quantite = quantite;
            this.libelle = libelle;
        }

        @Override
        public String toString() {
            return "Libelle : "+libelle+", Quantité : "+quantite;
        }
    }
}