package fr.univ_lille1.iutinfo.mfapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

public class AffichageActivity extends AppCompatActivity {
    private WebView mWebView = null;
    private String responseAffich = "reponse de base";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //String email = savedInstanceState.getString("email");
        final TextView txt = (TextView) findViewById(R.id.test);
        //final TextView txtEmail = (TextView) findViewById(R.id.tvEmail);
        //txtEmail.setText(email);
        String retour = getReq();
        Log.d("Ligne de retour ","§§§§§§§§"+responseAffich);
        //postReq();

    }

    public void onClick(View view){
        final TextView txt = (TextView) findViewById(R.id.test);
        txt.setText(responseAffich);
    }

    public String getReq(){

        RequestQueue queue = Volley.newRequestQueue(this);
        final TextView txt = (TextView) findViewById(R.id.test);

        final String url = "http://172.18.49.22:8080/v1/produit"; //10.0.2.2  172.18.49.19
        Response.Listener<String> getListReq = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                responseAffich = response;
                Log.d("REponse affich",responseAffich);

                txt.setText(response);
                //txt.setText("Resultat");
            }
        };

        Response.ErrorListener errorGetReq = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String response= "Request GET ERROR" + error.getCause();
                Log.d("Error.Response", response);
                txt.setText("Erreur");
            }
        };

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, getListReq,errorGetReq);

        // add it to the RequestQueue
        queue.add(getRequest);
        String resultat = txt.getText().toString();
        txt.setText("");
        return resultat;
    }

    public void postReq(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://172.18.49.22:8080/v1/user";
        final TextView txt = (TextView) findViewById(R.id.test);


        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                txt.setText("Reponse json");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                txt.setText("Error json");
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id","54");
                params.put("name", "MalauJu");
                params.put("alias", "alias");
                params.put("email","eamil");
                params.put("password","1234");
                return new JSONObject(params).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(sr);
    }
}
