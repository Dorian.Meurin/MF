package fr.univ_lille1.iutinfo.mfapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import static android.app.PendingIntent.getActivity;

/**
 * Created by boudonm on 23/03/18.
 */

public class Inscription extends AppCompatActivity  {

    private boolean passwordValide = false;
    private boolean uniqueEmail = false;
    private boolean textFill = false;
    public static Inscription inscription ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        TextView title = (TextView) findViewById(R.id.textView2);
        //title.setTextColor(0xFF008080);
        title.setTextColor(0x66FF1493);
        Button monButton=(Button)findViewById(R.id.envoie);
        //monButton.setBackgroundColor(0xFF66CDAA);
        monButton.setBackgroundColor(0x77FFC0CB);
    }

    /**Check if the password and the confirm password are equals**/
    public void verifMDP(){
        TextView error = (TextView) findViewById(R.id.error);
        EditText password1 =  (EditText) findViewById(R.id.password1);
        EditText password2 =  (EditText) findViewById(R.id.password2);

        if(password1.getText().toString().equals(password2.getText().toString())){
            passwordValide=true;
        }
        else{
            passwordValide=false;
            error.setText("- Les deux champs de mot de passe ne sont pas les mêmes \n");
        }

        if(password1.toString().length()<4){
            passwordValide=false;
            error.setText("- Le mot de passe doit contenir au moins 4 charactéres\n");
        }
    }

    /**Check if the email user is unique**/
    public void checkEmail(){
        TextView error = (TextView) findViewById(R.id.error);
        EditText email =  (EditText) findViewById(R.id.email);
        /*Requete req = new Requete(Volley.newRequestQueue(this));

        req.getByEmail(email.getText().toString());
        User user= new User();
        if(user.getEmail() == null){
            uniqueEmail=true;
        }
        else{
            uniqueEmail=false;
            error.setText("- L'email saisi est déja utilisé \n");
        }*/
        boolean presenceAro=false;
        for(int cpt=0;cpt<email.getText().toString().length();cpt++){
            if(email.getText().toString().charAt(cpt)=='@'){
                presenceAro=true;
            }
        }
        if(presenceAro==false){
            uniqueEmail=false;
            error.setText("- L'email ne contient pas d'@ \n");
        }
        else{
            uniqueEmail=true;
        }
    }

    /**Check are the text is filled**/
    public void checkFillText(){
        EditText name =  (EditText) findViewById(R.id.nom);
        EditText firstname =  (EditText) findViewById(R.id.prenom);
        EditText email =  (EditText) findViewById(R.id.email);
        EditText password1 =  (EditText) findViewById(R.id.password1);
        EditText password2 =  (EditText) findViewById(R.id.password2);


        if( estVide(name.getText(),"nom")){
            textFill=false;
        }
        if(estVide(firstname.getText(),"prenom")){
            textFill=false;
        }
        if(estVide(email.getText(),"email")){
            textFill=false;
        }
        if(estVide(password1.getText(),"premier mot de passe")){
            textFill=false;
        }
        if(estVide(password2.getText(),"second mot de passe")){
            textFill=false;
        }
        else{
            textFill=true;
        }
    }

    /**Return true if the argument is empty**/
    public boolean estVide(android.text.Editable t, String nom){
        TextView error = (TextView) findViewById(R.id.error);
        if(t.toString().equals("")){
            error.setText(error.getText()+"- Le champs "+ nom + " est vide \n");
            return true;
        }
        return false;
    }

    public void EnvoieInscritpion(View view){
        EditText name =  (EditText) findViewById(R.id.nom);
        EditText firstname =  (EditText) findViewById(R.id.prenom);
        EditText email =  (EditText) findViewById(R.id.email);
        EditText password1 =  (EditText) findViewById(R.id.password1);
        Requete req = new Requete(Volley.newRequestQueue(this));
        checkFillText();
        if(textFill==true){
            verifMDP();
            checkEmail();
            //uniqueEmail=true;
            if(passwordValide==true && uniqueEmail==true){

                User.nom=name.getText().toString();
                User.prenom=firstname.getText().toString();
                User.email=email.getText().toString();
                User.psw=password1.getText().toString();
                //Envoie du nouvelle utilisateur
               req.postUser(name.getText().toString(), firstname.getText().toString(), password1.getText().toString(), email.getText().toString());
                inscription=this;
                Intent intent = new Intent(this.getApplicationContext(), AffichList.class);
               //Intent intent = new Intent(Inscription.this, AffichList.class);
               startActivity(intent);
               // finish();
            }
            else{
            }
        }
        else{
        }
    }
}
