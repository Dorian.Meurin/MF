package fr.univ_lille1.iutinfo.mfapplication;

/**
 * Created by pierrut on 26/03/18.
 */

public class Kit {


        int id_kit;
        int id_personne;
        String libelle;

        public Kit(int id_kit, int id_personne, String libelle){
            this.id_kit = id_kit;
            this.id_personne = id_personne;
            this.libelle = libelle;
        }

        public String toString() {
            return "Libelle : "+libelle+" id_kit "+id_kit+" id_personne "+id_personne;
        }

}
