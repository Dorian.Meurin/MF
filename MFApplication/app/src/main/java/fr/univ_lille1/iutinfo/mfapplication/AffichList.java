package fr.univ_lille1.iutinfo.mfapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AffichList extends ListActivity {
    private ListView mListView;
    private ArrayList<Kit> l = new ArrayList<>();
    public String chaineJson = "base";
    public String urlServ = "http://172.18.49.20:8080";
    private boolean pressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affich_list);
        Button monButton=(Button)findViewById(R.id.buttonAfficher);
        monButton.setBackgroundColor(0x77FFC0CB);
        TextView nom = (TextView) findViewById(R.id.textView);
        nom.setTextColor(0x66FFFFF0);
        getJSON();
    }

    protected void onListItemClick(ListView l, View v , int position, long id){
        Log.e("Position/id",position+"/"+id);
        Log.e("Kit : ",this.l.get(position).toString());
        Kit kit = this.l.get(position);
        Intent intent = new Intent(this,AffichKit.class);
        intent.putExtra("id",kit.id_kit);
        intent.putExtra("personne",kit.id_personne);
        intent.putExtra("libelle",kit.libelle);
        startActivity(intent);
    }

    /*private void afficheListe2(String json){
        mListView = (ListView) findViewById(android.R.id.list);
        KitViewHolder viewHolder = (KitViewHolder) mListView.getTag();
        if(viewHolder == null){
            viewHolder = new KitViewHolder();
            viewHolder.avatar = (ImageView) mListView.findViewById(R.id.avatar);
            viewHolder.idPersonne = (TextView) mListView.findViewById(R.id.idPersonne);
            viewHolder.avatar = (ImageView) mListView.findViewById(R.id.avatar);
        }
    }*/

    private void afficheListe(String json){
        JSONArray js = null;
        try {
            js = new JSONArray(json);
            Log.d("Affichage de l'id",js.getJSONObject(1).toString());
        } catch (JSONException e) {
            Log.d("Erreur de jsonexception",e.getMessage());
            e.printStackTrace();
        }
        mListView = (ListView) findViewById(android.R.id.list);
        for(int i = 0; i < js.length(); i++){
            JSONObject jObject = null;
            try {
                jObject = js.getJSONObject(i);
                l.add(new Kit(jObject.getInt("id_kit"),jObject.getInt("id_personne"),jObject.getString("libelle")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        /*l.add(new Kit(1,1,"Premier kit"));
        l.add(new Kit(2,1,"Deuxieme kit"));
        l.add(new Kit(3,2,"Troisieme kit"));
        l.add(new Kit(4,4,json));*/

        final ArrayAdapter<Kit> adapter = new ArrayAdapter<Kit>(AffichList.this,
                android.R.layout.simple_list_item_1, l);
        mListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }




    public void onClick(View view) throws JSONException {
        if(!pressed) {
            pressed = true;
            afficheListe(chaineJson);
        }
    }

    public String getJSON(){
        final String[] resultat = new String[]{""};
        RequestQueue queue = Volley.newRequestQueue(this);
        final TextView mTxtJSON = (TextView) findViewById(R.id.textView);

        final String url = urlServ+"/v1/kit/kit"; //10.0.2.2  172.18.49.19
        Response.Listener<String> getListReq = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ResponseJSON", response);
                resultat[0] = response;
                chaineJson = response;
                mTxtJSON.setText(response);
            }
        };

        Response.ErrorListener errorGetReq = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String response= "Request GET ERROR" + error.getCause();
                Log.d("Error.Response", response);
                resultat[0] = response;
                mTxtJSON.setText(response);
                chaineJson = response;
            }
        };

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, getListReq,errorGetReq);

        // add it to the RequestQueue
        queue.add(getRequest);
        return resultat[0];
    }



    public class AfficheTask extends AsyncTask<Void, Void, String> {
        String json;

        AfficheTask() {
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return null;
            }
            json = getJSON();
            Log.d("affichage de jsonnnn",json);
            return json;
            // TODO: register the new account here.
            //return true;
        }

        @Override
        protected void onPostExecute(String chaine) {
        /* Execution */



        //finish();

        }


        @Override
        protected void onCancelled() {
            /*mAuthTask = null;
            showProgress(false);*/
        }
    }
    public void onCreerKit(View view){
        Intent intent = new Intent(AffichList.this, CreerKit.class);
        startActivity(intent);
    }
}
