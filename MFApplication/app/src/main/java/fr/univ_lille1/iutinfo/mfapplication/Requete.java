package fr.univ_lille1.iutinfo.mfapplication;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pierrut on 22/03/18.
 */

public class Requete extends AppCompatActivity {

    private RequestQueue queue ;

    public Requete(RequestQueue q){
        queue=q;
    }

    public void getByEmail(String email){

        String url = "http://172.18.49.20:8080/v1/personne/email/" + email;

        Response.Listener<String> list = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("ResponseJSON", response);
            }
        };

        Response.ErrorListener errorList = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Requete.this, "Erreur email get", Toast.LENGTH_LONG).show();
            }
        };

        StringRequest request = new StringRequest(url,list,errorList);
        queue.add(request);
    }

    public void postUser(String nom, String prenom, String psw, String mail){

        Map<String , String> post = new HashMap<>();
        post.put("email_personne",mail);
        post.put("nom_personne",nom);
        post.put("prenom_personne", prenom);
        post.put("passwdHash", psw);
        post.put("role_personne","CREATEUR");

        String url = "http://172.18.49.20:8080/v1/personne";

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(post), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
              //  Toast.makeText(Requete.this, "ça marche l'insertion de user", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(Requete.this,"ça marche pas insert user", Toast.LENGTH_LONG).show();
                TextView error1 = (TextView) findViewById(R.id.error);
                error1.setText(error1.getText()+"- Cet email est déjà utilisé \n");
            }
        });
        queue.add(json);

    }
}
