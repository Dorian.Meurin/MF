
select 'tous les produits'; 
SELECT * FROM produit ; 

SELECT 'toute les commandes' ; 
SELECT * 
	FROM commande as c,
	(SELECT * 
		FROM client as client , personne as p , adresse as a1, adresse as a2  
		WHERE client.id_personne = p.id_personne 
		AND client.id_adrsFacturation = a1.id_adresse 
		AND client.id_adrsLivraison = a2.id_adresse) as cl 
	WHERE cl.id_personne = cl.id_personne 
	AND   cl.id_personne = 1 ; 

SELECT 'produit d une commande ' ;

SELECT * 
	FROM detailCommande as cmd , 
		 produit as i
	WHERE cmd.id_item = i.id_produit
	AND cmd.id_item in (SELECT id_item 
						FROM item 
						WHERE type = 'PRODUIT' );
SELECT 'kit d une commande ';

SELECT * 
	FROM detailCommande as dtl , item as i 
	WHERE dtl.id_item = i.id_item 
	AND i.type in  ('KIT', 'PRODUIT')
	AND dtl.id_cmd = 1 ; 



/*SELECT id_produit, id_fournisseur, libelle, qteParArticle, stock, poidPackaging, prixPackaging, refFourniseurArticle
	FROM detailCommande as dtl, item as i, produit as p 
	WHERE i.id_item = p.id_produit 
	AND dtl.id_item = i.id_item 
	AND i.type = 'PRODUIT'
	AND dtl.id_cmd = 1;*/



SELECT 'detailKit' ;
SELECT * 
	FROM kitComposant as kit,
		 produit as p
	WHERE kit.id_produit = p.id_produit 
	AND kit.id_kit = 1 ; 

SELECT 'tous les clients';
SELECT * 
		FROM client as c, personne as p , adresse as a1, adresse as a2  
		WHERE c.id_personne = p.id_personne 
		AND c.id_adrsFacturation = a1.id_adresse 
		AND c.id_adrsLivraison = a2.id_adresse; 

SELECT 'tous les fournisseurs ';

SELECT * 
	FROM fournisseur as f, personne as p, adresse as a 
	WHERE f.id_personne = p.id_personne 
	AND   f.id_adres = a.id_adresse ; 

SELECT 'Tous les produits avec detail de kit ' ; 
